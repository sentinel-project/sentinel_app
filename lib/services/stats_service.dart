import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:sembast/sembast.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/models/stats/stats_model.dart';
import 'package:sentinel/setup.dart';

const StatEndpoint = ApiBase + "/stats";
const NoStats = 'No stats';

class StatService {
  Auth token;
  Database db;
  Stats stats;

  StatService({this.token, this.db});
  Future<bool> islocalcache(String type) async {
    try {
      if (type == "evo") {
        stats = await Stats.loadEvoStats(db);
      } else {
        stats = await Stats.loadBalStats(db);
      }
      return stats != null;
    } catch (e) {
      return false;
    }
  }

  Future<Stats> getBalancesStats() async {
    var res = await http.get(StatEndpoint, headers: token.header);
    var body = json.decode(res.body);

    if (res.statusCode == 200) {
      if (body == null) {
        throw Exception(NoStats);
      }
      stats = Stats.fromJson(body);
      return stats;
    } else {
      throw Exception('Failed to load stats');
    }
  }

  Future<bool> updateBalanceStats() async {
    try {
      stats = await getBalancesStats();
      stats.saveBalanceStats(db, stats);
      return true;
    } catch (e) {
      print("updateBalanceStats failed : " + e.toString());
      return false;
    }
  }

  Future<bool> updateEvolutionStats() async {
    try {
      stats = await getEvolutionStats();
      var res = await stats.saveEvolutionStats(db, stats);
      return res;
    } catch (e) {
      print("updateEvolutionStats failed : " + e.toString());
      return false;
    }
  }

  Future<Stats> getEvolutionStats() async {
    var res = await http.get(StatEndpoint + "/evo", headers: token.header);
    var body = json.decode(res.body);

    if (res.statusCode == 200) {
      if (body == null) {
        throw Exception(NoStats);
      }
      stats = Stats.fromJson(body);
      return stats;
    } else {
      throw Exception('Failed to load stats');
    }
  }
}
