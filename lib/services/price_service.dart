import 'dart:convert';

import 'package:sembast/sembast.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/models/prices/prices_model.dart';
import 'package:http/http.dart' as http;
import 'package:sentinel/setup.dart';


const Endpoint = ApiBase + "/prices";

class PriceService {
  Auth token;
  Prices prices;
  Database db;

  PriceService({this.token, this.db});
  Future<Map<String, dynamic>> updatePrices() async {
    final response =
        await http.get(Endpoint + "/update", headers: token.header);

    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return json.decode(response.body);
    } else {
      // If that response was not OK, throw an error.
      throw Exception(
          'Failed to refresh prices: ' + json.decode(response.body)['error']);
    }
  }

  Future<Prices> getPrices() async {
    final response = await http.get(Endpoint, headers: token.header);

    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Prices.fromJson(json.decode(response.body));
    } else {
      // If that response was not OK, throw an error.
      throw Exception(
          'Failed to load prices: ' + json.decode(response.body)['error']);
    }
  }

  Future<Prices> getPricesBySymbol(String symbol) async {
    final requrl = Endpoint + "/by/sym/" + symbol.trim();
    final response = await http.get(requrl, headers: token.header);

    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return Prices.fromJson(json.decode(response.body));
    } else {
      print(response.body);
      // If that response was not OK, throw an error.
      throw Exception(
          'Failed to load prices: ' + json.decode(response.body)['error']);
    }
  }

  Future<bool> updatePricesBySymbol(String symbol) async {
    try {
      Prices nprices = await getPricesBySymbol(symbol);
      nprices.savePriceBySymbol(db, symbol, nprices);
      return true;
    } catch (e) {
      print("updatepricebysymlbol failed : " + e.toString());
      throw "Failed to update prices";
    }
  }

  Future<bool> istherePriceSaved(String sym) async {
    try {
      prices = await Prices.loadPricesBySymbol(db, sym);
      return prices != null;
    } catch (e) {
      print("No prices saved for sym " + sym);
      return false;
    }
  }
}
