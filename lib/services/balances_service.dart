import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:sembast/sembast.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/models/balances/balances_model.dart';
import 'package:sentinel/setup.dart';

const Endpoint = ApiBase + "/balance";

class NoBalException implements Exception {
  String errMsg() => 'No balances in cache';
}

class BalancesService {
  Auth token;
  Balances bals;
  Database db;

  BalancesService({this.token, this.db});
  Future<bool> islocalcache() async {
    try {
      bals = await Balances.load(db);
      return bals != null;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<Balances> getBalances() async {
    final response = await http.get(Endpoint, headers: token.header);
    var body = json.decode(response.body);

    if (response.statusCode == 200) {
      if (body == null) {
        throw NoBalException;
      }
      bals = Balances.fromJson(body);
      return bals;
    } else {
      throw NoBalException;
    }
  }

  Future<bool> updateBalances() async {
    try {
      bals = await getBalances();
      bals.save(db, bals);
      return true;
    } catch (e) {
      return false;
    }
  }
}
