import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:sembast/sembast.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/models/user/user_model.dart';
import 'package:sentinel/setup.dart';

const UserEndpoint = ApiBase + "/user";

class UserService {
  Auth token;
  User user;
  Database db;

  UserService({this.token, this.db});

  Future<bool> islocalcache() async {
    bool islogged = false;
    try {
      user = await User.load(db);
      islogged = user != null;
      // Checking if is logged user is user in cache
      if (islogged) {
        islogged = user.name == token.payload.name;
      }
      return islogged;
    } catch (e) {
      return islogged;
    }
  }

  Future<User> getUser() async {
    final response = await http.get(UserEndpoint, headers: token.header);

    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return User.fromJson(json.decode(response.body));
    } else {
      // If that response was not OK, throw an error.
      var err = json.decode(response.body);
      throw Exception(err["error"]);
    }
  }

  Future<Map<String, dynamic>> createUser(User nuser) async {
    final jsondata = json.encode(nuser.toJson());
    final response = await http.post(UserEndpoint, body: jsondata);

    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      return json.decode(response.body);
    } else {
      // If that response was not OK, throw an error.
      throw Exception(
          'Failed to load user: ' + json.decode(response.body)["error"]);
    }
  }
}
