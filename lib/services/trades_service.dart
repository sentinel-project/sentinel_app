import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/models/trades/trades_model.dart';
import 'package:sentinel/setup.dart';

const TradesEndpoint = ApiBase + "/trades";

class TradesService {
  Auth token;

  TradesService({this.token});
  Future<Trades> getAllTrades() async {
    var response = await http.get(TradesEndpoint, headers: token.header);
    var body = json.decode(response.body);
    if (response.statusCode == 200) {
      if (body == null) {
        throw Exception('No trades data available');
      }
      return Trades.fromJson(body);
    } else {
      throw body["error"];
    }
  }

  Future<Map<String, dynamic>> updateTradesBySymbol(String sym) async {
    var furl = TradesEndpoint + "/update/by/sym/" + sym;
    var response = await http.get(furl, headers: token.header);
    var body = json.decode(response.body);
    if (response.statusCode == 200) {
      if (body == null) {
        throw Exception('No trades data available');
      }
      return body;
    } else {
      throw body.toString();
    }
  }
}
