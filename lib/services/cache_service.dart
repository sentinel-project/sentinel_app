import 'package:sembast/sembast.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/models/user/user_model.dart';

class CacheService {
  Auth token;
  Database db;
  List<String> storesRefs = ["balance", "prices", "quotes", "stats", "user"];

  CacheService({this.token, this.db});

  Future<bool> _clear() async {
    final store = StoreRef.main();
    for (String ref in storesRefs) {
      try {
        await store.record(ref).delete(db);
      } catch (e) {
        return false;
      }
    }
    return true;
  }

  Future<bool> checkCache() async {
    var cachedUser = await User.load(db);
    if (token.payload.name != cachedUser.name) {
      print("coiocoi");
      print(token);
      var iserr = await _clear();
      return iserr;
    }
    return true;
  }
}
