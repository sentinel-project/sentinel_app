import 'dart:convert';

import 'package:sembast/sembast.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/models/quotes/quotes_model.dart';
import 'package:http/http.dart' as http;
import 'package:sentinel/setup.dart';

const QuoteEndpoint = ApiBase + "/quotes";

class QuoteService {
  Auth token;
  Database db;
  Quotes quotes;

  QuoteService({this.token, this.db});
  Future<bool> islocalcache(String sym) async {
    try {
      quotes = await Quotes.loadBySymbol(db, sym);
      return quotes != null;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> updateQuotesBySymbol(String sym) async {
    try {
      quotes = await getQuotesBySymbol(sym);
      var res = await Quotes.saveBySymbol(db, quotes, sym);
      print(res);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<dynamic> updateQuotes() async {
    var requrl = QuoteEndpoint + "/update";
    var res = await http.get(requrl, headers: token.header);
    if (res.statusCode == 200) {
      return json.decode(res.body);
    } else {
      throw Exception(json.decode(res.body)["error"]);
    }
  }

  Future<Quotes> getQuotesBySymbol(String sym) async {
    var requrl = QuoteEndpoint + "/by/sym/" + sym;
    var res = await http.get(requrl, headers: token.header);
    if (res.statusCode == 200 && json.decode(res.body) != null) {
      return Quotes.fromJson(json.decode(res.body));
    } else {
      throw Exception('failed to get quotes with symbol ' + sym);
    }
  }

  Future<Quotes> getQuotesByAsset(String asset) async {
    var requrl = QuoteEndpoint + "/by/asset/" + asset;
    var res = await http.get(requrl, headers: token.header);
    if (res.statusCode == 200) {
      return Quotes.fromJson(json.decode(res.body));
    } else {
      throw Exception('failed to get quotes with asset ' + asset);
    }
  }
}
