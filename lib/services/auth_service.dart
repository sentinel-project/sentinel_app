import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:sembast/sembast.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/setup.dart';

const AuthEndpoint = ApiBase + "/auth";

class AuthService {
  Auth token;
  Database db;

  AuthService({this.token, this.db});

  Future<bool> islocalcache() async {
    try {
      token = await Auth.load(db);
      var exist = token != null;
      var now = (DateTime.now().millisecondsSinceEpoch / 1000); 
      var isexp =  now > token.payload.exp;
      // TODO: add error logging instead of true false
      return exist && !isexp;
    } catch (e) {
      print(e);
      return false;
    }
  }

  static Future<Auth> login(String name, String password) async {
    final creds = {"name": name, "password": password};
    try {
      final resp = await http.post(AuthEndpoint, body: json.encode(creds));
      if (resp.statusCode == 200) {
        return Auth.fromJson(json.decode(resp.body));
      } else {
        throw "Invalid login";
      }
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<Auth> getloggedin() async {
    try {
      var token = await Auth.load(db);
      print(token.jwt);
      return token;
    } catch (e) {
      throw Exception("failed to load auth datas");
    }
  }

  // static Future<bool> refreshToken()
}
