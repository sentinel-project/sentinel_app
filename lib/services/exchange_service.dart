import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/models/exchanges/exchange_model.dart';

import 'package:sentinel/models/exchanges/exchanges_model.dart';
import 'package:sentinel/setup.dart';

const Endpoint = ApiBase + "/exchange";

class ExchangeService {
  Auth token;

  ExchangeService({this.token});
  Future<Map<String, String>> addExchange(Exchange nexchange) async {
    final response = await http.post(
      Endpoint,
      headers: token.header,
      body: json.encode(nexchange.toJson()),
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to add exchange');
    }
  }

  Future<Exchanges> getExchanges() async {
    final response = await http.get(Endpoint, headers: token.header);

    if (response.statusCode == 200) {
      return Exchanges.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load exchanges');
    }
  }
}
