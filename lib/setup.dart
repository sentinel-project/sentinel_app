library sentinel.setup;

// The api endpoint to be updated here
// Prod A
const String ApiBase = "https://blooming-ocean-31863.herokuapp.com";
// Dev Tethering
// const String ApiBase = "http://192.168.43.72:3000";
// Dev adb reverse
// const String ApiBase = "http://localhost:3000";
// Dev localnetwork
// const String ApiBase = "http://192.168.1.11:3000";
