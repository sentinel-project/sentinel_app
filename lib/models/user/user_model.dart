import 'dart:convert';

import 'package:sembast/sembast.dart';

class NoUserException implements Exception { 
   String errMsg() => 'No user in cache'; 
} 
class User {
  final String name;
  final String coverImage;
  final String email;
  final String password;
  final String jwt;

  User({this.name, this.coverImage, this.email, this.password, this.jwt});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        name: json['name'],
        coverImage: json['cover_image'],
        email: json['email'],
        password: json['password']);
  }

  Map<String, String> toJson() => {
        'name': name,
        'email': email,
        'cover_image': coverImage,
        'password': password
      };
  static Future<bool> save(Database db, User user) async {
    final store = StoreRef.main();
    try {
      await store.record('user').put(db, json.encode(user), merge: true);
      return true;
    } catch (e) {
      return false;
    }
  }

  static Future<User> load(Database db) async {
    final store = StoreRef.main();
    try {
      var record = await store.record('user').get(db) as String;
      var obj = json.decode(record);
      return new User.fromJson(obj);
    } catch (e) {
      // print(e);
      throw NoUserException;
    }
  }
}
