import 'dart:convert';

import 'package:sembast/sembast.dart';

class Sum {
  final String base;
  final double value;

  Sum({this.base, this.value});
  factory Sum.fromJson(Map<String, dynamic> json) {
    var price = double.parse(json['value'].toString());
    return Sum(base: json['base'], value: price);
  }
  Map<String, dynamic> toJson() {
    return {"base": base, "value": value};
  }
}

class Stats {
  final Sum sum;
  final List<Sum> minmax;
  final List<Sum> ranks;
  Stats({this.sum, this.minmax, this.ranks});
  factory Stats.fromJson(Map<String, dynamic> json) {
    List<Sum> lminmax = List<Sum>();
    List<Sum> lranks = List<Sum>();
    List<dynamic> mmx = json["minmax"];
    List<dynamic> rks = json["ranks"];
    Sum lsum = Sum.fromJson(json["sum"]);
    lminmax = mmx.map((s) => Sum.fromJson(s)).toList();
    lranks = rks.map((s) => Sum.fromJson(s)).toList();
    return Stats(sum: lsum, minmax: lminmax, ranks: lranks);
  }
  Map<String, dynamic> toJson() {
    return {
      "sum": sum.toJson(),
      "minmax": minmax.map((f) => f.toJson()).toList(),
      "ranks": ranks.map((f) => f.toJson()).toList()
    };
  }

  Future<bool> saveBalanceStats(Database db, Stats st) async {
    final store = StoreRef.main();
    try {
      await store
          .record('stats' + '.balance')
          .put(db, json.encode(st), merge: true);
      return true;
    } catch (e) {
      print("error save balancestats: " + e.toString());
      return false;
    }
  }

  Future<bool> saveEvolutionStats(Database db, Stats st) async {
    final store = StoreRef.main();
    try {
      await store
          .record('stats' + '.evolution')
          .put(db, json.encode(st), merge: true);
      return true;
    } catch (e) {
      print("error save evostats: " + e.toString());
      return false;
    }
  }

  static Future<Stats> loadBalStats(Database db) async {
    final store = StoreRef.main();
    try {
      var record = await store.record('stats' + '.balance').get(db) as String;
      var obj = json.decode(record);
      return new Stats.fromJson(obj);
    } catch (e) {
      print("LoadBalStats : " + e.toString());
      throw Exception('Error loading Balance stats from memory');
    }
  }

  static Future<Stats> loadEvoStats(Database db) async {
    final store = StoreRef.main();
    try {
      var record = await store.record('stats' + '.evolution').get(db) as String;
      var obj = json.decode(record);
      return new Stats.fromJson(obj);
    } catch (e) {
      print("LoadEvoStats : " + e.toString());
      throw Exception('Error loading Evolution stats from memory');
    }
  }
}
