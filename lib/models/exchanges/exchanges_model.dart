import 'package:sentinel/models/exchanges/exchange_model.dart';

class Exchanges {
  final List<Exchange> exchanges;
  int get length => exchanges.length;
  int len() => exchanges.length;

  Exchanges({this.exchanges});

  factory Exchanges.fromJson(List<dynamic> json) {
    List<Exchange> exchs = List<Exchange>();
    exchs = json.map((e) => Exchange.fromJson(e)).toList();
    return Exchanges(exchanges: exchs);
  }
  Exchange operator [](int i) => exchanges[i];
}
