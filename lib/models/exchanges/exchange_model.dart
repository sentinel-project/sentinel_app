class Exchange {
  final String id;
  final String name;
  final String userId;
  final String publicKey;
  final String privateKey;

  Exchange({this.id, this.name, this.userId, this.publicKey, this.privateKey});
  factory Exchange.fromJson(Map<String, dynamic> json) {
    return Exchange(
        id: json['id'],
        name: json['name'],
        userId: json['uid'],
        publicKey: json['pubkey'],
        privateKey: json['privkey']);
  }
  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "name": name,
      "pubkey": publicKey,
      "privkey": privateKey,
    };
  }
}
