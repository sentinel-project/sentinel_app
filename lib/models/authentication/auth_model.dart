import 'dart:convert';

import 'package:sembast/sembast.dart';
import 'package:sentinel/models/authentication/jwtpayload_model.dart';

/// Decode JWT base64 encoded payloads
/// (big thanks to [StackOverflow](https://stackoverflow.com/questions/52017389/how-to-get-the-claims-from-a-jwt-in-my-flutter-application?rq=1))
String _decodeBase64(String str) {
  String output = str.replaceAll('-', '+').replaceAll('_', '/');
  switch (output.length % 4) {
    case 0:
      break;
    case 2:
      output += '==';
      break;
    case 3:
      output += '=';
      break;
    default:
      throw Exception('Illegal base64url string!"');
  }

  return utf8.decode(base64Url.decode(output));
}

/// Parse jwt string (the token)
Map<String, dynamic> parseJwt(String token) {
  final parts = token.split('.');
  if (parts.length != 3) {
    throw Exception('invalid token');
  }

  final payload = _decodeBase64(parts[1]);
  final payloadMap = json.decode(payload);
  if (payloadMap is! Map<String, dynamic>) {
    throw Exception('invalid payload');
  }

  return payloadMap;
}

class Auth {
  String jwt;
  JWTPayload payload;
  Map<String, String> header;

  Auth({this.jwt, this.payload, this.header});
  factory Auth.fromJson(Map<String, dynamic> json) {
    var _jwt = json["token"];
    var _jwtheader = json["type"] + " " + _jwt;
    var _payload = parseJwt(json["token"]);
    JWTPayload _data = JWTPayload.fromJson(_payload);
    Map<String, String> _headers = {"Authorization": _jwtheader};
    return Auth(
      jwt: _jwt,
      header: _headers,
      payload: _data,
    );
  }
  Map<String, dynamic> toJson() {
    return {"token": jwt, "type": "Bearer"};
  }

  static Future<bool> saveAuth(Database db, Auth token) async {
    final store = StoreRef.main();
    try {
      await store.record('auth').put(db, json.encode(token), merge: true);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  static Future<Auth> load(Database db) async {
    final store = StoreRef.main();
    try {
      var record = await store.record('auth').get(db) as String;
      var obj = json.decode(record);
      return new Auth.fromJson(obj);
    } catch (e) {
      print(e);
      throw Exception('Error loading auth datas from memory');
    }
  }
}
