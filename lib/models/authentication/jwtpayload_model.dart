/// The sentinel api jwt token (payload part) format
class JWTPayload {
  /// Token expiration
  int exp;

  /// Token issuer (sentinel api)
  String iss;

  /// Unique user subject
  String sub;

  /// User name
  String name;

  /// User Agent
  String ua;

  /// Last IP addr
  String ip;
  JWTPayload({this.exp, this.iss, this.sub, this.name, this.ua, this.ip});
  factory JWTPayload.fromJson(Map<String, dynamic> json) {
    return JWTPayload(
        exp: json["exp"],
        iss: json["iss"],
        sub: json["sub"],
        name: json["name"],
        ua: json["ua"],
        ip: json["ip"]);
  }

  Map<String, dynamic> toJson() {
    return {
      "exp": exp,
      "iss": iss,
      "sub": sub,
      "name": name,
      "ua": ua,
      "ip": ip
    };
  }
}
