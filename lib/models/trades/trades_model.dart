class Trade {
  final String symbol;
  final double quantity;
  final double sum;
  final double averagePrice;
  final String type;
  final String side;
  final String date;
  Trade(
      {this.symbol,
      this.quantity,
      this.sum,
      this.averagePrice,
      this.type,
      this.side,
      this.date});
  factory Trade.fromJson(Map<String, dynamic> json) {
    return Trade(
        symbol: json["symbol"],
        quantity: double.parse(json["quantity"].toString()),
        sum: double.parse(json["sum"].toString()),
        averagePrice: double.parse(json["avg_price"].toString()),
        type: json["type"],
        side: json["side"],
        date: json["date"]);
  }
}

class Trades {
  final List<Trade> trades;
  Trades({this.trades});
  factory Trades.fromJson(List<dynamic> json) {
    List<Trade> ltrades = List<Trade>();
    ltrades = json.map((t) => Trade.fromJson(t)).toList();
    return Trades(trades: ltrades);
  }
}
