class Price {
  final String symbol;
  final String exchangeId;
  final double price;
  final String date;
  Price({this.symbol, this.exchangeId, this.price, this.date});

  factory Price.fromJson(Map<String, dynamic> json) {
    return Price(
        symbol: json['symbol'],
        exchangeId: json['eid'],
        price: double.parse(json['price'].toString()),
        date: json['date']);
  }

  Map<String, dynamic> toJson() {
    return {"symbol": symbol, "eid": exchangeId, "price": price, "date": date};
  }
}
