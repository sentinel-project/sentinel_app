import 'dart:convert';

import 'package:sembast/sembast.dart';
import 'package:sentinel/models/prices/price_model.dart';

class Prices {
  final List<Price> prices;
  Prices({this.prices});
  factory Prices.fromJson(List<dynamic> parsedJson) {
    List<Price> prs = List<Price>();
    prs = parsedJson.map((p) => Price.fromJson(p)).toList();
    return Prices(prices: prs);
  }
  Price operator [](int i) => prices[i];
  int len() => prices.length;

  Future<bool> savePriceBySymbol(Database db, String sym, Prices pr) async {
    final store = StoreRef.main();
    try {
      await store
          .record('prices' + '.' + sym.trim().toLowerCase())
          .put(db, json.encode(pr.prices));
      return true;
    } catch (e) {
      print('savePriceBySymbol : ' + e.toString());
      return false;
    }
  }

  static Future<Prices> loadPricesBySymbol(Database db, String sym) async {
    final store = StoreRef.main();
    try {
      var record = await store
          .record('prices' + '.' + sym.trim().toLowerCase())
          .get(db) as String;
      var obj = json.decode(record);
      return new Prices.fromJson(obj);
    } catch (e) {
      print("LoadPriceBySymbol failed : " + e.toString());
      throw Exception('Error loading Prices from memory');
    }
  }
}
