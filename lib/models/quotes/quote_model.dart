class Quote {
  final String pair;
  final double value;
  final String assetId;
  final String exchangeId;
  final String date;

  Quote({this.pair, this.value, this.assetId, this.exchangeId, this.date});
  factory Quote.fromJson(Map<String, dynamic> json) {
    return Quote(
        pair: json['pair'],
        value: double.parse(json['value'].toString()),
        assetId: json['aid'],
        exchangeId: json['eid'],
        date: json['date']);
  }
  Map<String, dynamic> toJson() {
    return {
      "pair": pair,
      "value": value,
      "aid": assetId,
      "eid": exchangeId,
      "date": date
    };
  }
}
