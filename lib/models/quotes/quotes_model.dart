import 'dart:convert';

import 'package:sembast/sembast.dart';
import 'package:sentinel/models/quotes/quote_model.dart';

class Quotes {
  final List<Quote> quotes;
  Quotes({this.quotes});
  factory Quotes.fromJson(List<dynamic> json) {
    List<Quote> qts = List<Quote>();
    qts = json.map((q) => Quote.fromJson(q)).toList();
    return Quotes(quotes: qts);
  }
  Quote operator [](int i) => quotes[i];
  int len() => quotes.length;
  static Future<bool> saveBySymbol(
      Database db, Quotes quotes, String sym) async {
    final store = StoreRef.main();
    try {
      await store
          .record('quotes' + '.' + sym.trim().toLowerCase())
          .put(db, json.encode(quotes.quotes), merge: true);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  static Future<Quotes> loadBySymbol(Database db, String sym) async {
    final store = StoreRef.main();
    try {
      var record = await store
          .record('quotes' + '.' + sym.trim().toLowerCase())
          .get(db) as String;
      var obj = json.decode(record);
      return new Quotes.fromJson(obj);
    } catch (e) {
      throw Exception('Error loading quotes from memory');
    }
  }
}
