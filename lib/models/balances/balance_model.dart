class Balance {
  final String id;
  final String asset;
  final double free;
  final double locked;
  final List<String> markets;
  final String updatedAt;

  Balance(
      {this.id,
      this.asset,
      this.free,
      this.locked,
      this.markets,
      this.updatedAt});

  factory Balance.fromJson(Map<String, dynamic> json) {
    return Balance(
        id: json['id'],
        asset: json['asset'],
        free: double.parse(json['free'].toString()),
        locked: double.parse(json['locked'].toString()),
        markets: List<String>.from(json['markets']),
        updatedAt: json['lastupdate']);
  }
  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "asset": asset,
      "free": free,
      "locked": locked,
      "markets": markets,
      "lastupdate": updatedAt
    };
  }
}
