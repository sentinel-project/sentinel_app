import 'dart:convert';

import 'package:sembast/sembast.dart';
import 'package:sentinel/models/balances/balance_model.dart';

class Balances {
  final List<Balance> balances;
  Balances({this.balances});
  factory Balances.fromJson(List<dynamic> parsedJson) {
    List<Balance> bals = List<Balance>();
    bals = parsedJson.map((i) => Balance.fromJson(i)).toList();

    return Balances(
      balances: bals,
    );
  }
  Balance operator [](int i) => balances[i];
  int len() => balances.length;
  Future<bool> save(Database db, Balances bal) async {
    final store = StoreRef.main();
    try {
      await store
          .record('balance')
          .put(db, json.encode(bal.balances), merge: true);
      return true;
    } catch (e) {
      return false;
    }
  }

  static Future<Balances> load(Database db) async {
    final store = StoreRef.main();
    try {
      var record = await store.record('balance').get(db) as String;
      var obj = json.decode(record);
      return new Balances.fromJson(obj);
    } catch (e) {
      throw Exception('Error loading balances from memory');
    }
  }
}
