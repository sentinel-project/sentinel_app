import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';

import 'login/login_page.dart';

class Sentinel extends StatefulWidget {
  Sentinel({Key key}) : super(key: key);

  @override
  _SentinelState createState() => _SentinelState();
}

class _SentinelState extends State<Sentinel> {
  // Setting default theme to black
  Brightness brightness = Brightness.dark;
  // File path to a file in the current directory
  String dbPath = '/sentinel.db';
  DatabaseFactory dbFactory = databaseFactoryIo;
  // We use the database factory to open the database
  Database db;

  void init() async {
    try {
      var directory = await getApplicationDocumentsDirectory();
      var path = directory.path;
      print(path);
      db = await dbFactory.openDatabase(path + dbPath);
      setState(() {});
    } catch (e) {
      print(e);
    }
  }

  @override
  initState() {
    super.initState();
    init();
  }

  function(value) => setState(() {
        brightness = value;
      });
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sentinel',
      debugShowCheckedModeBanner: false,

      theme:
          brightness == Brightness.dark ? ThemeData.dark() : ThemeData.light(),
      home: db != null
          ? LoginPage(
              title: 'Sentinel',
              brightness: brightness,
              cb: function,
              db: db,
            )
          : Center(
              child: CircularProgressIndicator(),
            ),
    );
  }
}

class SentinelApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Sentinel();
  }
}
