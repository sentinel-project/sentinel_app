import 'package:flutter/material.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/models/user/user_model.dart';
import 'package:sentinel/widgets/login/login_page.dart';
import 'package:sentinel/widgets/user/exchanges/addexchange_page.dart';

class OnboardingPage extends StatefulWidget {
  final Function cb;
  final Auth token;
  final User user;
  final Brightness brightness;
  OnboardingPage({Key key, this.cb, this.token, this.user, this.brightness})
      : super(key: key);

  @override
  _OnboardingPageState createState() => _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPage> {
  String err;
  Brightness ness;
  bool darkmode;
  bool isvalidate;
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    super.dispose();
  }

  @override
  initState() {
    super.initState();
    darkmode = widget.brightness == Brightness.dark;
    ness = widget.brightness;
    isvalidate = false;
    setState(() {});
  }

  updateTheme(bool val) {
    darkmode = val;
    setState(() {});
    ness = darkmode ? Brightness.dark : Brightness.light;
    widget.cb(ness);
  }

  isValidate(value) => setState(() {
        isvalidate = value;
      });

  @override
  Widget build(BuildContext context) {
    final _title = Row(
      children: <Widget>[
        SizedBox(
          width: 10,
        ),
        Text("Sentinel"),
        Expanded(
          child: Container(),
        ),
        Text(
          "💡",
          textScaleFactor: 0.8,
        ),
        Switch(
          onChanged: updateTheme,
          value: darkmode,
        ),
        Text(
          "🌙",
          textScaleFactor: 0.8,
        ),
      ],
    );
    return Scaffold(
      appBar: AppBar(
        title: _title,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              Column(
                children: <Widget>[
                  SizedBox(
                    height: 18,
                  ),
                  Text(
                    "🚀 Welcome aboard ",
                    textScaleFactor: 2,
                  ),
                  Text(
                    widget.user.name[0].toUpperCase() +
                        widget.user.name.substring(1),
                    textScaleFactor: 1.8,
                  ),
                  SizedBox(
                    height: 18,
                  )
                ],
              ),
              isvalidate == false
                  ? AddExchangePage(
                      token: widget.token,
                      cb: isValidate,
                    )
                  : FlatButton.icon(
                      icon: Icon(Icons.arrow_right),
                      label: Text("Next"),
                      onPressed: () => Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => LoginPage(
                                    cb: widget.cb,
                                    brightness: ness,
                                  ))),
                    )
            ],
          ),
        ],
      ),
    );
  }
}
