import 'package:flutter/material.dart';
import 'package:sembast/sembast.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/models/stats/stats_model.dart';
import 'package:sentinel/services/stats_service.dart';

class BalancesStatsPage extends StatefulWidget {
  final Auth token;
  final bool darkmode;
  final Function cb;
  final Database db;

  BalancesStatsPage({Key key, this.token, this.db, this.darkmode, this.cb})
      : super(key: key);
  @override
  _StatsPageState createState() => _StatsPageState();
}

class _StatsPageState extends State<BalancesStatsPage> {
  String errmsg;
  Stats stats;
  bool isloading;

  Future<void> init() async {
    Stats st;
    try {
      bool iscached = await StatService(db: widget.db, token: widget.token)
          .islocalcache("bal");
      if (!iscached) {
        st = await StatService(token: widget.token).getBalancesStats();
      } else {
        st = await Stats.loadBalStats(widget.db);
      }
      stats = st;
    } catch (e) {
      errmsg = e.toString();
    }
  }

  @override
  initState() {
    super.initState();
    init().then((onValue) => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    if (stats != null) {
      return Expanded(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                FlatButton.icon(
                  icon: Icon(Icons.refresh),
                  onPressed: () => StatService(
                          token: widget.token, db: widget.db)
                      .updateBalanceStats()
                      .then((v) => init().then((onValue) => setState(() {}))),
                  label: Text("Update"),
                ),
                Text(
                  stats.sum.value.toStringAsFixed(4) + " " + stats.sum.base,
                  textScaleFactor: 3.2,
                ),
              ],
            ),
            Expanded(
                child: ListView.builder(
                    itemCount: stats.ranks.length,
                    itemBuilder: (ctx, i) {
                      var perc = (stats.ranks[i].value * 100) / stats.sum.value;
                      return Padding(
                        padding: EdgeInsets.all(10),
                        child: Card(
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.all(10),
                                child: RaisedButton.icon(
                                  label:
                                      Text(stats.ranks[i].base.split('-')[1]),
                                  icon: Icon(Icons.add_comment),
                                  onPressed: () {},
                                ),
                              ),
                              Expanded(
                                child: Container(),
                              ),
                              Padding(
                                  padding: EdgeInsets.all(10),
                                  child: Text(
                                      stats.ranks[i].value.toStringAsFixed(6) +
                                          " BTC")),
                              Expanded(
                                child: Container(),
                              ),
                              Padding(
                                padding: EdgeInsets.all(10),
                                child: Text(perc.toStringAsFixed(2) + " %"),
                              ),
                            ],
                          ),
                        ),
                      );
                    })),
          ],
        ),
      );
    }
    return Center(
      child: errmsg == null
          ? CircularProgressIndicator()
          : Padding(
              padding: EdgeInsets.all(10),
              child: Text(" ⚠ " + " : [" + errmsg + "]"),
            ),
    );
  }
}
