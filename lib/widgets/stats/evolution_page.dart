import 'package:flutter/material.dart';
import 'package:sembast/sembast.dart';
import 'package:sentinel/models/stats/stats_model.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/services/stats_service.dart';

class EvoStatsPage extends StatefulWidget {
  final Auth token;
  final bool darkmode;
  final Function cb;
  final Database db;

  EvoStatsPage({Key key, this.token, this.db, this.darkmode, this.cb})
      : super(key: key);
  @override
  _EvoStatsPageState createState() => _EvoStatsPageState();
}

class _EvoStatsPageState extends State<EvoStatsPage> {
  String errmsg;
  bool darkmode;
  Brightness ness;
  Stats stats;

  void init() async {
    Stats st;
    try {
      bool iscached = await StatService(db: widget.db, token: widget.token)
          .islocalcache("evo");
      if (!iscached) {
        st = await StatService(token: widget.token).getEvolutionStats();
      } else {
        st = await Stats.loadEvoStats(widget.db);
      }
      stats = st;
    } catch (e) {
      errmsg = e.toString();
    }
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  initState() {
    super.initState();
    darkmode = widget.darkmode;
    ness = darkmode ? Brightness.dark : Brightness.light;
    init();
  }

  @override
  Widget build(BuildContext context) {
    if (stats != null) {
      return Expanded(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FlatButton.icon(
                    icon: Icon(Icons.refresh),
                    onPressed: () =>
                        StatService(token: widget.token, db: widget.db)
                            .updateEvolutionStats()
                            .then((v) => init()),
                    label: Text("Update"),
                  ),
                  Text(
                    stats.sum.value.toString() +
                        " " +
                        stats.sum.base[0].toUpperCase() +
                        stats.sum.base.substring(1),
                    textScaleFactor: 1,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    stats.ranks.length.toString() + " " + "Symbols",
                    textScaleFactor: 1.2,
                  ),
                ],
              ),
            ),
            Expanded(
                child: ListView.builder(
                    itemCount: stats.ranks.length,
                    itemBuilder: (ctx, i) {
                      if (stats.ranks[i].value == 0) {
                        return Container();
                      }
                      return Padding(
                        padding: EdgeInsets.all(10),
                        child: Card(
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.all(10),
                                child: RaisedButton.icon(
                                  label: Text(stats.ranks[i].base),
                                  icon: Icon(Icons.add_comment),
                                  onPressed: () {},
                                ),
                              ),
                              Expanded(
                                child: Container(),
                              ),
                              Padding(
                                  padding: EdgeInsets.all(10),
                                  child: RaisedButton.icon(
                                    onPressed: () {},
                                    color: stats.ranks[i].value > 0
                                        ? Colors.green
                                        : Colors.redAccent,
                                    icon: Icon(stats.ranks[i].value > 0
                                        ? Icons.monetization_on
                                        : Icons.money_off),
                                    label: Text(
                                      stats.ranks[i].value.toStringAsFixed(4) +
                                          " %",
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      );
                    })),
          ],
        ),
      );
    }
    return Center(
      child: errmsg == null
          ? CircularProgressIndicator()
          : Padding(
              padding: EdgeInsets.all(10),
              child: Text(" ⚠ " + " : [" + errmsg + "]"),
            ),
    );
  }
}
