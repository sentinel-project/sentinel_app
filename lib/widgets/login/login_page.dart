import 'package:flutter/material.dart';
import 'package:local_auth/auth_strings.dart';
import 'package:local_auth/local_auth.dart';
import 'package:sembast/sembast.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/models/user/user_model.dart';
import 'package:sentinel/services/auth_service.dart';
import 'package:sentinel/services/cache_service.dart';
import 'package:sentinel/widgets/register/register_page.dart';
import 'package:sentinel/widgets/user/user_page.dart';

class LoginPage extends StatefulWidget {
  final Function cb;
  final Brightness brightness;
  final String title;
  final Database db;
  LoginPage({Key key, this.cb, this.title, this.brightness, this.db})
      : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Auth token;
  String err;
  Brightness ness;
  static bool bioauth;
  static bool isloading;
  static bool isallreadylogged;
  static bool darkmode;
  static final nameController = TextEditingController();
  static final passwordController = TextEditingController();
  final name = TextFormField(
    controller: nameController,
    keyboardType: TextInputType.text,
    autofocus: false,
    decoration: InputDecoration(
      hintText: 'Username',
      contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
    ),
  );

  final password = TextFormField(
    controller: passwordController,
    autofocus: false,
    obscureText: true,
    decoration: InputDecoration(
      hintText: 'Password',
      contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
    ),
  );

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    nameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  void checkbio() async {
    try {
      var localAuth = new LocalAuthentication();
      bioauth = await localAuth.authenticateWithBiometrics(
        useErrorDialogs: false,
        localizedReason: 'Please touch sensor to authenticate',
        androidAuthStrings: AndroidAuthMessages(signInTitle: "Sentinel check"),
      );
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => UserPage(
                  token: token,
                  cb: widget.cb,
                  db: widget.db,
                  darkmode: darkmode)));
    } catch (e) {
      print(e);
    }
  }

  void _attemptLogin() async {
    setState(() {
      isloading = true;
    });
    try {
      token = await AuthService.login(
          name.controller.text, password.controller.text);
      bool res = await Auth.saveAuth(widget.db, token);
      await CacheService(db: widget.db, token: token).checkCache();
      print("login page res");
      print(res);
      err = "";
    } catch (e) {
      var errmsg;

      // TODO: handle error with nice messages :)
      if (e.toString().contains("Exception: ")) {
        errmsg = e.toString().split("Exception: ")[1];
        if (errmsg == "Socket") {
          errmsg = " 👿 Trouble connecting ...";
        }
      } else {
        errmsg = e.toString();
      }
      err = errmsg;
    }
    isloading = false;
    setState(() {});
  }

  void init() async {
    isloading = false;
    try {
      isallreadylogged = await AuthService(db: widget.db).islocalcache();
      if (isallreadylogged) {
        token = await Auth.load(widget.db);
        setState(() {});
        checkbio();
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  initState() {
    super.initState();
    darkmode = widget.brightness == Brightness.dark;
    ness = widget.brightness;
    setState(() {});
    init();
  }

  void updateTheme(bool val) {
    darkmode = val;
    setState(() {});
    ness = darkmode ? Brightness.dark : Brightness.light;
    widget.cb(ness);
  }

  @override
  Widget build(BuildContext context) {
    final _title = Row(
      children: <Widget>[
        SizedBox(
          width: 10,
        ),
        Text("Sentinel"),
        Expanded(
          child: Container(),
        ),
        Text(
          "💡",
          textScaleFactor: 0.8,
        ),
        Switch(
          onChanged: updateTheme,
          value: darkmode,
        ),
        Text(
          "🌙",
          textScaleFactor: 0.8,
        ),
      ],
    );
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.fingerprint),
        mini: true,
        onPressed: checkbio,
      ),
      appBar: AppBar(
        title: _title,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              name,
              SizedBox(height: 8.0),
              password,
              SizedBox(height: 8.0),
              Center(
                  child: err != null && err != ""
                      ? Padding(
                          padding: EdgeInsets.all(10),
                          child: Text(" ⚠ " + " : [" + err + "]"),
                        )
                      : Container()),
              // TODO: A better state handling
              FlatButton.icon(
                icon:
                    isloading ? CircularProgressIndicator() : Icon(Icons.input),
                label: Text(isloading ? "" : "Login"),
                onPressed: _attemptLogin,
              ),
              FlatButton(
                child: Text(
                  "Don't have an account ? Please register here",
                  style: TextStyle(color: Colors.blueGrey),
                ),
                onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => RegisterPage(
                              cb: widget.cb,
                              brightness: ness,
                            ))),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
