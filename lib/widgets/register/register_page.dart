import 'package:flutter/material.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/models/user/user_model.dart';
import 'package:sentinel/services/user_service.dart';
import 'package:sentinel/widgets/login/login_page.dart';

class RegisterPage extends StatefulWidget {
  final Function cb;
  final Brightness brightness;
  final String title;
  RegisterPage({Key key, this.cb, this.title, this.brightness})
      : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  Auth token;
  String err;
  Brightness ness;
  bool darkmode;
  bool isvalidate;
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    nameController.dispose();
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  initState() {
    super.initState();
    darkmode = widget.brightness == Brightness.dark;
    ness = widget.brightness;
    isvalidate = false;
    setState(() {});
  }

  updateTheme(bool val) {
    darkmode = val;
    setState(() {});
    ness = darkmode ? Brightness.dark : Brightness.light;
    widget.cb(ness);
  }

  @override
  Widget build(BuildContext context) {
    final name = TextFormField(
      controller: nameController,
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Username',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final password = TextFormField(
      controller: passwordController,
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final email = TextFormField(
      controller: emailController,
      autofocus: false,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
    final _title = Row(
      children: <Widget>[
        SizedBox(
          width: 10,
        ),
        Text("Sentinel"),
        Expanded(
          child: Container(),
        ),
        Text(
          "💡",
          textScaleFactor: 0.8,
        ),
        Switch(
          onChanged: updateTheme,
          value: darkmode,
        ),
        Text(
          "🌙",
          textScaleFactor: 0.8,
        ),
      ],
    );
    return Scaffold(
      appBar: AppBar(
        title: _title,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              SizedBox(
                height: 10.0,
              ),
              name,
              SizedBox(height: 8.0),
              email,
              SizedBox(height: 8.0),
              password,
              SizedBox(height: 8.0),
              Center(
                  child: err != null && err != ""
                      ? Padding(
                          padding: EdgeInsets.all(10),
                          child: Text(" ⚠ " +
                              " : [" +
                              err.split("Exception: ")[1] +
                              "]"),
                        )
                      : Container()),
              SizedBox(height: 24.0),
              isvalidate == false
                  ? FlatButton.icon(
                      label: Text("Create profile"),
                      icon: Icon(Icons.check),
                      onPressed: () async {
                        var nuser = User(
                            name: name.controller.text,
                            email: email.controller.text,
                            password: password.controller.text,
                            coverImage: "notyet");
                        try {
                          var r = await UserService().createUser(nuser);
                          print(r);
                          setState(() {
                            isvalidate = true;
                          });
                        } catch (e) {
                          print(e);
                        }
                      },
                    )
                  : FlatButton.icon(
                      icon: Icon(Icons.input),
                      label: Text("Login"),
                      onPressed: () => Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => LoginPage(
                                    cb: widget.cb,
                                    brightness: ness,
                                  ))),
                    )
            ],
          ),
        ],
      ),
    );
  }
}
