import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:sentinel/models/quotes/quotes_model.dart';

class QuotesChartWidget extends StatelessWidget {
  final Quotes data;

  QuotesChartWidget({this.data});

  List<FlSpot> getSpots(Quotes dta) {
    final List<FlSpot> foo = List<FlSpot>();
    var i = 0;
    for (; i < dta.quotes.length; ++i) {
      var rnd = data.quotes[i].value.toStringAsFixed(6);
      var fii = FlSpot(i.toDouble(), double.parse(rnd));
      foo.add(fii);
    }
    return foo;
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 260,
      width: 330,
      child: Padding(
        // padding: const EdgeInsets.only(right: 16.0, left: 0.0),
        padding: EdgeInsets.only(top: 10, left: 10),
        child: LineChart(
            LineChartData(
              lineTouchData: LineTouchData(
                  // touchResponseSink: controller.sink,
                  touchTooltipData: LineTouchTooltipData(
                tooltipBgColor: Colors.blueGrey.withOpacity(0.8),
              )),
              gridData: FlGridData(
                show: true,
              ),
              titlesData: FlTitlesData(
                bottomTitles: SideTitles(
                  showTitles: true,
                  reservedSize: 22,
                  textStyle: TextStyle(
                    color: const Color(0xff72719b),
                    fontWeight: FontWeight.bold,
                    fontSize: 6,
                  ),
                  margin: 10,
                  getTitles: (value) {
                    // TODO: fix the charts ;)
                    var date = data.quotes[value.toInt()].date.split("T")[0];
                    if (value.toInt() % 10 == 0) {
                      return date;
                    }
                    return '';
                  },
                ),
                leftTitles: SideTitles(
                  showTitles: true,
                  textStyle: TextStyle(
                    color: Color(0xff75729e),
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                  ),
                  getTitles: (value) {
                    // print(value.toStringAsFixed(6));
                    return '';
                  },
                  margin: 8,
                  reservedSize: 30,
                ),
              ),
              borderData: FlBorderData(
                  show: true,
                  border: Border(
                    bottom: BorderSide(
                      color: Color(0xff4e4965),
                      width: 4,
                    ),
                    left: BorderSide(
                      color: Colors.transparent,
                    ),
                    right: BorderSide(
                      color: Colors.transparent,
                    ),
                    top: BorderSide(
                      color: Colors.transparent,
                    ),
                  )),
              lineBarsData: [
                LineChartBarData(
                  spots: getSpots(data),
                  isCurved: false,
                  colors: [
                    Color(0xff4af699),
                  ],
                  barWidth: 8,
                  isStrokeCapRound: true,
                  dotData: FlDotData(
                    show: false,
                  ),
                  belowBarData: BarAreaData(
                    show: false,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
  }
}
