import 'package:flutter/material.dart';
import 'package:sentinel/models/quotes/quotes_model.dart';

class QuotesWidget extends StatelessWidget {
  final Quotes data;

  QuotesWidget({this.data});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 260,
      child: ListView.builder(
        itemCount: data.len(),
        itemBuilder: (ctx, i) {
          return Card(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(data[i].pair),
                Text(data[i].value.toString()),
                Text(data[i].date),
              ],
            ),
          );
        },
      ),
    );
  }
}
