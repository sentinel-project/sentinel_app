import 'package:flutter/material.dart';
import 'package:sembast/sembast.dart';
import 'package:sentinel/models/balances/balance_model.dart';
import 'package:sentinel/models/quotes/quotes_model.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/services/quote_service.dart';
import 'package:sentinel/widgets/user/quotes/quotes_widget.dart';
import 'package:sentinel/widgets/user/quotes/quoteschart_widget.dart';

class QuotesPage extends StatefulWidget {
  final Auth token;
  final Balance balance;
  final Function cb;
  final bool darkmode;
  final Database db;

  QuotesPage(
      {Key key, this.darkmode, this.cb, this.db, this.token, this.balance})
      : super(key: key);
  @override
  _QuotesPageState createState() => _QuotesPageState();
}

class _QuotesPageState extends State<QuotesPage> {
  int page;
  String err;
  Quotes quotes;
  Brightness ness;
  bool darkmode;
  bool isupdating;
  String dropdownValue;

  @override
  initState() {
    super.initState();
    darkmode = widget.darkmode;
    dropdownValue = widget.balance.markets[0];
    isupdating = false;
    ness = darkmode ? Brightness.dark : Brightness.light;
    init();
  }

  void init() async {
    Quotes st;
    try {
      bool iscached = await QuoteService(db: widget.db, token: widget.token)
          .islocalcache(dropdownValue);
      if (!iscached) {
        st = await QuoteService(token: widget.token)
            .getQuotesBySymbol(dropdownValue);
      } else {
        st = await Quotes.loadBySymbol(widget.db, dropdownValue);
      }
      quotes = st;
    } catch (e) {
      err = e.toString();
    }
    setState(() {});
  }

  Widget getEvolutionRate(Quotes dta) {
    var evo = dta[dta.len() - 1].value - dta[0].value;
    var perc = (evo / dta[0].value) * 100;
    var evostr = " " + (perc < 0 ? "-" : "+") + perc.toStringAsFixed(4);
    var msg = widget.balance.asset + " " + widget.balance.free.toString();

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(10),
          child: Text(msg),
        ),
        Padding(
          padding: EdgeInsets.all(10),
          child: Text(
            evostr,
            style: TextStyle(
                color: (perc < 0 ? Colors.redAccent : Colors.greenAccent)),
          ),
        )
      ],
    );
  }

  List<DropdownMenuItem<String>> getItems(List<String> markets) {
    var ddown = markets.map<DropdownMenuItem<String>>((String value) {
      return DropdownMenuItem<String>(
        value: value,
        child: Text(value),
      );
    });
    return ddown.toList();
  }

  @override
  Widget build(BuildContext context) {
    final _title = Row(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(right: 10),
          child: Text("SYM."),
        ),
        DropdownButton<String>(
          value: dropdownValue,
          onChanged: (String newValue) {
            dropdownValue = newValue;
            setState(() {});
            init();
          },
          items: getItems(widget.balance.markets),
        ),
        Expanded(
          child: Container(),
        ),
        Text(
          "💡",
          textScaleFactor: 0.8,
        ),
        Switch(
          onChanged: (val) {
            darkmode = val;
            ness = darkmode ? Brightness.dark : Brightness.light;
            widget.cb(ness);
          },
          value: darkmode,
        ),
        Text(
          "🌙",
          textScaleFactor: 0.8,
        ),
      ],
    );
    final _chartactionrow = Row(
      children: <Widget>[
        FlatButton.icon(
          icon: Icon(Icons.verified_user),
          onPressed: () {},
          label: Container(),
        ),
        FlatButton.icon(
          icon: Icon(Icons.wallpaper),
          onPressed: () {},
          label: Container(),
        ),
        DropdownButton(
          value: "all",
          onChanged: (val) {},
          items: [
            DropdownMenuItem(
              value: "all",
              child: Text("all"),
            ),
            DropdownMenuItem(
              value: "1w",
              child: Text("1 Week"),
            ),
            DropdownMenuItem(
              value: "1m",
              child: Text("1 Month"),
            ),
            DropdownMenuItem(
              value: "1y",
              child: Text("1 Month"),
            )
          ],
        ),
        FlatButton.icon(
          icon: isupdating
              ? CircularProgressIndicator(
                  strokeWidth: 1,
                )
              : Icon(Icons.save),
          onPressed: () {
            isupdating = true;
            setState(() {});
            QuoteService(token: widget.token, db: widget.db)
                .updateQuotesBySymbol(dropdownValue)
                .then((v) {
              print(v);
              isupdating = false;
              setState(() {});
              init();
            });
          },
          label: Container(),
        ),
      ],
    );

    if (quotes != null) {
      return Scaffold(
        appBar: AppBar(
          title: _title,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Padding(
                padding: EdgeInsets.all(5),
                child: getEvolutionRate(quotes),
              ),
            ),
            QuotesChartWidget(
              data: quotes,
            ),
            _chartactionrow,
            QuotesWidget(
              data: quotes,
            )
          ],
        ),
      );
    }
    return Scaffold(
      body: err == null
          ? CircularProgressIndicator()
          : Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  " ⚠ ",
                  textScaleFactor: 5,
                ),
                Padding(
                  padding: EdgeInsets.all(20),
                  child: Text(
                    "[" + err + "]",
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ),
                RaisedButton.icon(
                  icon: Icon(Icons.arrow_back),
                  label: Text("Go back"),
                  onPressed: () => Navigator.pop(context),
                )
              ],
            ),
    );
  }
}
