import 'package:flutter/material.dart';
import 'package:sembast/sembast.dart';
import 'package:sentinel/models/balances/balance_model.dart';
import 'package:sentinel/models/balances/balances_model.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/services/balances_service.dart';

import 'balance_widget.dart';

class BalancesPage extends StatefulWidget {
  final Auth token;
  final Function cb;
  final bool darkmode;
  final Database db;
  BalancesPage({Key key, this.token, this.db, this.darkmode, this.cb})
      : super(key: key);
  @override
  _BalancesPageState createState() => _BalancesPageState();
}

List<DropdownMenuItem<String>> getItemsFromStrings(List<String> strings) {
  var ddown = strings.map<DropdownMenuItem<String>>((String value) {
    return DropdownMenuItem<String>(
      value: value,
      child: Text(value),
    );
  });
  return ddown.toList();
}

List<String> getAssetList(Balances bals) {
  var els = List<String>();
  els.add("ALL");
  var al = bals.balances.map((f) => f.asset).toList();
  els.addAll(al);
  return els;
}

class _BalancesPageState extends State<BalancesPage> {
  String errmsg;
  bool darkmode;
  Brightness ness;
  Balances bals;
  List<Balance> fbals;
  String choosenAsset;
  List<String> assetlist;
  List<DropdownMenuItem<String>> itmlist;

  void init() async {
    Balances st;
    try {
      bool iscached = await BalancesService(db: widget.db, token: widget.token)
          .islocalcache();
      print(iscached);
      if (!iscached) {
        st = await BalancesService(token: widget.token).getBalances();
      } else {
        st = await Balances.load(widget.db);
      }
      bals = st;
      fbals = bals.balances;
      choosenAsset = bals[0].asset;
      assetlist = getAssetList(bals);
      itmlist = getItemsFromStrings(assetlist);
    } catch (e) {
      print(e);
      errmsg = e.toString();
    }
    setState(() {});
  }

  Map<String, dynamic> getStatsMap(Balances b) {
    int m = 0;
    var a = 0;
    if (b != null) {
      a = b.len();
      for (var j = 0; j < b.len(); ++j) {
        m += b[j].markets.length;
      }
    }
    return {"markets": m, "assets": a};
  }

  Widget getActionsBar() {
    var stats = getStatsMap(bals);
    var txtB = " Markets : " + stats["markets"].toString();
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: DropdownButton<String>(
            hint: Text("Symbols"),
            value: choosenAsset,
            isDense: true,
            items: itmlist,
            onChanged: (String nval) => setState(() {
              choosenAsset = nval;
              if (nval != "ALL") {
                fbals = [
                  bals.balances.firstWhere((a) => a.asset == choosenAsset)
                ];
              } else {
                fbals = bals.balances;
              }
            }),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: Text(
            txtB,
            textScaleFactor: 1,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: FlatButton.icon(
            icon: Icon(Icons.save),
            onPressed: () {
              BalancesService(token: widget.token, db: widget.db)
                  .updateBalances()
                  .then((v) {
                print(v);
                init();
              });
            },
            label: Text("Update"),
          ),
        ),
      ],
    );
  }

  @override
  initState() {
    super.initState();
    darkmode = widget.darkmode;
    ness = darkmode ? Brightness.dark : Brightness.light;
    init();
  }

  @override
  Widget build(BuildContext context) {
    if (bals != null) {
      return Expanded(
        child: Column(
          children: <Widget>[
            getActionsBar(),
            SizedBox(
              height: 5,
            ),
            Expanded(
              child: ListView.builder(
                itemCount: fbals.length,
                itemBuilder: (ctx, i) => SizedBox(
                  height: 220,
                  child: BalanceWidget(
                    bal: fbals[i],
                    token: widget.token,
                    cb: widget.cb,
                    darkmode: darkmode,
                    db: widget.db,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }
    return Center(
      child: errmsg == null
          ? CircularProgressIndicator()
          : Padding(
              padding: EdgeInsets.all(10),
              child: Text(" ⚠ " + " : [" + errmsg + "]"),
            ),
    );
  }
}
