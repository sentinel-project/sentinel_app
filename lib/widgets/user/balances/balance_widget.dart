import 'package:flutter/material.dart';
import 'package:sembast/sembast.dart';
import 'package:sentinel/models/balances/balance_model.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/widgets/user/prices/pricechart_page.dart';
import 'package:sentinel/widgets/user/prices/pricechart_widget.dart';
import 'package:sentinel/widgets/user/quotes/quotes_page.dart';

class BalanceWidget extends StatelessWidget {
  final Balance bal;
  final Auth token;
  final bool darkmode;
  final Function cb;
  final Database db;

  BalanceWidget({this.bal, this.darkmode, this.token, this.cb, this.db});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      margin: EdgeInsets.all(9),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 200,
                child: Container(
                  decoration: BoxDecoration(color: Colors.blueAccent),
                  child: Center(
                    child: RotatedBox(
                      quarterTurns: 1,
                      child: Text(
                        bal.asset,
                        textScaleFactor: 1.6,
                        // style: TextStyle(color: Color(0xff4af699)),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(),
              ),
              PriceChartWidget(
                asset: bal,
              ),
              Expanded(
                child: Container(),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  bal.locked > 0
                      ? Text("🔒 : " + bal.locked.toString() + bal.asset[0])
                      : Container(),
                  SizedBox(
                    height: 5,
                  ),
                  Text("🔓 : " + bal.free.toStringAsFixed(5) + bal.asset[0]),
                  SizedBox(
                    height: 5,
                  ),
                  FlatButton.icon(
                    color: Colors.blueAccent,
                    icon: Icon(Icons.show_chart),
                    onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => QuotesPage(
                                  balance: bal,
                                  token: token,
                                  cb: cb,
                                  darkmode: darkmode,
                                  db: db,
                                ))),
                    label: Text(
                      'Quotes.',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  FlatButton.icon(
                    color: Colors.blueGrey,
                    icon: Icon(Icons.euro_symbol),
                    onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PriceChartsPage(
                                  balance: bal,
                                  token: token,
                                  cb: cb,
                                  darkmode: darkmode,
                                  db: db,
                                ))),
                    label: Text(
                      'Prices.',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  FlatButton.icon(
                    color: Colors.greenAccent,
                    icon: Icon(
                      Icons.settings,
                      color: Colors.white,
                    ),
                    onPressed: () {},
                    label: Text(
                      'Param.',
                      style: TextStyle(color: Colors.blueGrey),
                    ),
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
