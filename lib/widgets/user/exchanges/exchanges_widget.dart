import 'package:flutter/material.dart';
import 'package:sentinel/models/exchanges/exchanges_model.dart';
import 'package:sentinel/widgets/user/exchanges/exchange_widget.dart';

class ExchangesWidget extends StatelessWidget {
  final Exchanges exchanges;

  ExchangesWidget({this.exchanges});
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        itemCount: exchanges.length,
        itemBuilder: (ctx, i) => ExchangeWidget(
          exchange: exchanges[i],
        ),
      ),
    );
  }
}
