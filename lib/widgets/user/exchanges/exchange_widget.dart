import 'package:flutter/material.dart';
import 'package:sentinel/models/exchanges/exchange_model.dart';

class ExchangeWidget extends StatelessWidget {
  final Exchange exchange;

  ExchangeWidget({this.exchange});
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Text(exchange.name),
    );
  }
}
