import 'package:flutter/material.dart';
import 'package:sentinel/models/exchanges/exchange_model.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/services/exchange_service.dart';

class AddExchangePage extends StatefulWidget {
  final Auth token;
  final Function cb;
  AddExchangePage({Key key, this.token, this.cb}) : super(key: key);
  @override
  _AddExchangePageState createState() => _AddExchangePageState();
}

class _AddExchangePageState extends State<AddExchangePage> {
  Exchange nexchange;
  bool isloading;
  final nameController = TextEditingController();
  final pubkeyController = TextEditingController();
  final privkeyController = TextEditingController();

  @override
  initState() {
    super.initState();
    isloading = false;
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    nameController.dispose();
    pubkeyController.dispose();
    privkeyController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final name = TextFormField(
      controller: nameController,
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Exchange name',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final pubkey = TextFormField(
      controller: pubkeyController,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Public api key',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final privkey = TextFormField(
      controller: privkeyController,
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Private api key',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24, right: 24),
          children: <Widget>[
            name,
            SizedBox(
              height: 8.0,
            ),
            pubkey,
            SizedBox(
              height: 8.0,
            ),
            privkey,
            SizedBox(
              height: 8.0,
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: Text("1 - Add api credential to auto sync your balances"),
            ),
            isloading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : FlatButton.icon(
                    icon: Icon(Icons.add_circle),
                    label: Text("Add exchange"),
                    onPressed: () async {
                      nexchange = new Exchange(
                          name: name.controller.text,
                          publicKey: pubkey.controller.text,
                          privateKey: privkey.controller.text);
                      var r = await ExchangeService(token: widget.token)
                          .addExchange(
                        nexchange,
                      );
                      print(r);
                      widget.cb(true);
                    },
                  ),
          ],
        )
      ],
    );
  }
}
