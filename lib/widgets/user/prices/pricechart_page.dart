import 'package:flutter/material.dart';
import 'package:sembast/sembast.dart';
import 'package:sentinel/models/balances/balance_model.dart';
import 'package:sentinel/models/prices/prices_model.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/services/price_service.dart';
import 'package:sentinel/widgets/user/prices/prices_widget.dart';
import 'package:sentinel/widgets/user/prices/priceschart_widget.dart';

class PriceChartsPage extends StatefulWidget {
  final Auth token;
  final Balance balance;
  final Function cb;
  final bool darkmode;
  final Database db;
  PriceChartsPage(
      {Key key, this.db, this.darkmode, this.cb, this.token, this.balance})
      : super(key: key);
  @override
  _PriceChartsPageState createState() => _PriceChartsPageState();
}

class _PriceChartsPageState extends State<PriceChartsPage> {
  Prices prices;
  bool darkmode;
  bool isupdating;
  Brightness ness;
  String err;
  int page;
  String dropdownValue;

  @override
  initState() {
    super.initState();
    darkmode = widget.darkmode;
    dropdownValue = widget.balance.markets[0];
    ness = darkmode ? Brightness.dark : Brightness.light;
    isupdating = false;
    init();
  }

  void init() async {
    Prices p;
    try {
      bool iscached = await PriceService(token: widget.token, db: widget.db)
          .istherePriceSaved(dropdownValue);
      if (!iscached) {
        p = await PriceService(token: widget.token, db: widget.db)
            .getPricesBySymbol(dropdownValue);
      } else {
        p = await Prices.loadPricesBySymbol(widget.db, dropdownValue);
      }
      prices = p;
    } catch (e) {
      print(e);
      err = e.toString();
    }
    setState(() {});
  }

  Widget getEvolutionRate(Prices dta) {
    var evo = dta[dta.len() - 1].price - dta[0].price;
    var perc = (evo / dta[0].price) * 100;
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(10),
          child: Text("Evo : " + perc.toStringAsFixed(4)),
        )
      ],
    );
  }

  List<DropdownMenuItem<String>> getItems(List<String> markets) {
    var ddown = markets.map<DropdownMenuItem<String>>((String value) {
      return DropdownMenuItem<String>(
        value: value,
        child: Text(value),
      );
    });
    return ddown.toList();
  }

  @override
  Widget build(BuildContext context) {
    final _title = Row(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(right: 10),
          child: Text("SYM."),
        ),
        DropdownButton<String>(
          value: dropdownValue,
          onChanged: (String newValue) {
            dropdownValue = newValue;
            setState(() {});
            init();
          },
          items: getItems(widget.balance.markets),
        ),
        Expanded(
          child: Container(),
        ),
        Text(
          "💡",
          textScaleFactor: 0.8,
        ),
        Switch(
          onChanged: (val) {
            darkmode = val;
            ness = darkmode ? Brightness.dark : Brightness.light;
            widget.cb(ness);
          },
          value: darkmode,
        ),
        Text(
          "🌙",
          textScaleFactor: 0.8,
        ),
      ],
    );
    final _chartactionrow = Row(
      children: <Widget>[
        FlatButton.icon(
          icon: Icon(Icons.verified_user),
          onPressed: () {},
          label: Container(),
        ),
        FlatButton.icon(
          icon: Icon(Icons.wallpaper),
          onPressed: () {},
          label: Container(),
        ),
        DropdownButton(
          value: "all",
          onChanged: (val) {},
          items: [
            DropdownMenuItem(
              value: "all",
              child: Text("all"),
            ),
            DropdownMenuItem(
              value: "1w",
              child: Text("1 Week"),
            ),
            DropdownMenuItem(
              value: "1m",
              child: Text("1 Month"),
            ),
            DropdownMenuItem(
              value: "1y",
              child: Text("1 Month"),
            )
          ],
        ),
        FlatButton.icon(
          icon: isupdating
              ? CircularProgressIndicator(
                  strokeWidth: 1,
                )
              : Icon(Icons.sync),
          onPressed: () async {
            isupdating = true;
            try {
              var v = await PriceService(token: widget.token, db: widget.db)
                  .updatePricesBySymbol(dropdownValue);
              isupdating = false;
              if (v) {
                init();
              }
            } catch (e) {}
            setState(() {});
          },
          label: Container(),
        )
      ],
    );

    if (prices != null) {
      return Scaffold(
        appBar: AppBar(
          title: _title,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Padding(
                padding: EdgeInsets.all(5),
                child: getEvolutionRate(prices),
              ),
            ),
            PricesChartWidget(
              data: prices,
            ),
            _chartactionrow,
            PricesWidget(
              asset: widget.balance.asset,
              prices: prices,
            )
          ],
        ),
      );
    }
    return Scaffold(
      body: Center(
        child: err == null
            ? CircularProgressIndicator()
            : Column(
                children: <Widget>[Text(" ⚠ "), Text("[" + err + "]")],
              ),
      ),
    );
  }
}
