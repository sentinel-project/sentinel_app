import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:sentinel/models/balances/balance_model.dart';

class PriceChartWidget extends StatelessWidget {
  final Balance asset;

  PriceChartWidget({this.asset});

  List<FlSpot> getSpots(Balance dta) {
    final List<FlSpot> foo = List<FlSpot>();
    var i = 0;
    var fii = FlSpot(i.toDouble(), asset.free);
    foo.add(fii);
    foo.add(FlSpot(2, asset.locked));
    return foo;
  }

  @override
  Widget build(BuildContext context) {
     List<PieChartSectionData> pieChartRawSections;
  List<PieChartSectionData> showingSections;
    final section1 = PieChartSectionData(
      color: Colors.greenAccent,
      value: asset.free,
      title: "🔓 Free",
      radius: 80,
      titleStyle: TextStyle(fontSize:  15, fontWeight: FontWeight.bold),
      titlePositionPercentageOffset: 0.5,
    );

    final section2 = PieChartSectionData(
      color: Colors.blueGrey,
      value: asset.locked,
      title: "🔒 Locked",
      radius: 80,
      titleStyle: TextStyle(fontSize: asset.locked <= asset.free ? 10 : 18, fontWeight: FontWeight.bold),
      titlePositionPercentageOffset: 0.5,
    );
    final items = [
      section1,
      section2
    ];
      pieChartRawSections = items;

    showingSections = pieChartRawSections;
    return SizedBox(
      height: 160,
      width: 190,
      child: Padding(
        padding: const EdgeInsets.only(right: 16.0, left: 6.0),
        child: PieChart(PieChartData(
                      startDegreeOffset: 180,
                      borderData: FlBorderData(
                        show: false,
                      ),
                      sectionsSpace: 12,
                      centerSpaceRadius: 0,
                      sections: showingSections),
                  ),),
        );
  }
}
