import 'package:flutter/material.dart';
import 'package:sentinel/models/prices/prices_model.dart';

class PricesWidget extends StatelessWidget {
  final String asset;
  final Prices prices;

  PricesWidget({this.asset, this.prices});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 260,
      child: ListView.builder(
        itemCount: prices.len(),
        itemBuilder: (c, i) {
          var base = asset;
          var sym = prices[i].symbol.split(base.trim());
          var quote = sym.length > 1 ? sym[1] : sym[0];
          if (quote == "") {
            quote = sym[0];
          }
          return Card(
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(base + " "),
                  Text(quote),
                  Text(prices[i].price.toString()),
                  Text(prices[i].date),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
