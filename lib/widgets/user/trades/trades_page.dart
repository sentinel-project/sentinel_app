import 'package:flutter/material.dart';
import 'package:sentinel/models/trades/trades_model.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/services/trades_service.dart';
import 'package:sentinel/widgets/user/trades/tradessettings_page.dart';

class TradesPage extends StatefulWidget {
  final Auth token;
  final bool darkmode;
  final Function cb;

  TradesPage({Key key, this.token, this.darkmode, this.cb}) : super(key: key);
  @override
  _TradesPageState createState() => _TradesPageState();
}

class _TradesPageState extends State<TradesPage> {
  String errmsg;
  String symValue;
  String sideValue;
  bool showsetting;
  Trades trades;
  List<Trade> ftrades;

  void init() async {
    try {
      Trades st = await TradesService(token: widget.token).getAllTrades();
      trades = st;
      ftrades = st.trades;
    } catch (e) {
      print(e);
      errmsg = e.toString();
    }
    setState(() {});
  }

  @override
  initState() {
    super.initState();
    showsetting = false;
    symValue = "Any";
    sideValue = "ALL";
    init();
  }

  isupdated(bool val) {
    if (val) {
      init();
    }
  }

  List<String> getSymbolList(Trades trades) {
    var foo = List<String>();
    foo.add("Any");
    trades.trades.forEach((t) => foo.add(t.symbol));
    foo = foo.toSet().toList();
    return foo;
  }

  List<String> sideList = ["ALL", "BUY", "SELL"];

  filterBySymbol(String sym) {
    ftrades = trades.trades.where((t) => t.symbol.trim() == sym).toList();
  }

  filterBySide(String side) {
    ftrades = ftrades.where((t) => t.side == side).toList();
  }

  List<DropdownMenuItem<String>> getItemsFromStrings(List<String> strings) {
    var ddown = strings.map<DropdownMenuItem<String>>((String value) {
      return DropdownMenuItem<String>(
        value: value,
        child: Text(value),
      );
    });
    return ddown.toList();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: put 'tradesearch' text input even if there is no `ftrades`
    if (ftrades != null) {
      return Expanded(
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  "All trades",
                  textScaleFactor: 2,
                ),
                SizedBox(
                  width: 10,
                ),
                DropdownButton<String>(
                  value: symValue,
                  onChanged: (String newSymValue) {
                    symValue = newSymValue;

                    if (newSymValue != "Any") {
                      filterBySymbol(symValue);
                    } else {
                      ftrades = trades.trades;
                    }
                    setState(() {});
                  },
                  items: getItemsFromStrings(getSymbolList(trades)),
                ),
                Expanded(
                  child: Container(),
                ),
                DropdownButton<String>(
                  value: sideValue,
                  onChanged: (String newValue) {
                    sideValue = newValue;
                    if (newValue != "ALL") {
                      filterBySide(sideValue);
                    } else {
                      ftrades = trades.trades;
                    }
                    setState(() {});
                  },
                  items: getItemsFromStrings(["ALL", "BUY", "SELL"]),
                ),
                IconButton(
                  icon: Icon(Icons.update),
                  onPressed: () => setState(() {
                    showsetting = !showsetting;
                  }),
                )
              ],
            ),
            showsetting == true
                ? TradesSettingsPage(
                    token: widget.token,
                    cb: isupdated,
                  )
                : Container(),
            Expanded(
                child: ListView.builder(
                    itemCount: ftrades.length,
                    itemBuilder: (ctx, i) {
                      return Padding(
                        padding: EdgeInsets.all(10),
                        child: Card(
                          child: Row(
                            children: <Widget>[
                              Container(
                                  height: 118,
                                  width: 50,
                                  decoration: BoxDecoration(
                                      color: ftrades[i].side == "BUY"
                                          ? Colors.blueAccent
                                          : Colors.orange),
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(ftrades[i].side),
                                    ),
                                  )),
                              Expanded(
                                child: Container(),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(10),
                                child: Column(
                                  children: <Widget>[
                                    Text(ftrades[i].symbol),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(ftrades[i].quantity.toString() +
                                        " @  " +
                                        ftrades[i]
                                            .averagePrice
                                            .toStringAsFixed(8)),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text("Tot :" + ftrades[i].sum.toString()),
                                    Padding(
                                      padding: EdgeInsets.all(10),
                                      child: Text(
                                        DateTime.parse(ftrades[i].date)
                                            .toString(),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Container(),
                              ),
                              Column(
                                children: <Widget>[
                                  IconButton(
                                      icon: Icon(Icons.find_in_page),
                                      onPressed: () {}),
                                ],
                              )
                            ],
                          ),
                        ),
                      );
                    })),
          ],
        ),
      );
    }
    return Center(
      child: errmsg == null
          ? CircularProgressIndicator()
          : Padding(
              padding: EdgeInsets.all(10),
              child: Text(" ⚠ " + " : [" + errmsg + "]"),
            ),
    );
  }
}
