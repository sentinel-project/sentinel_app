import 'package:flutter/material.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/services/trades_service.dart';

class TradesSettingsPage extends StatefulWidget {
  final Auth token;
  final Function cb;

  TradesSettingsPage({Key key, this.token, this.cb}) : super(key: key);
  @override
  _TradesSettingsPageState createState() => _TradesSettingsPageState();
}

class _TradesSettingsPageState extends State<TradesSettingsPage> {
  String errmsg;
  String base;
  String quote;
  String pair;
  final baseController = TextEditingController();
  final quoteController = TextEditingController();
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    baseController.dispose();
    quoteController.dispose();
    super.dispose();
  }

  void update() async {
    try {
      base = baseController.text;
      quote = quoteController.text;
      pair = base.trim().toUpperCase() + quote.trim().toUpperCase();
      var res =
          await TradesService(token: widget.token).updateTradesBySymbol(pair);
      final snackBar = SnackBar(content: Text(res["success"]));
      Scaffold.of(context).showSnackBar(snackBar);
      widget.cb(true);
    } catch (e) {
      print(e);
      errmsg = e.toString();
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final base = TextFormField(
      controller: baseController,
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Base',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
    final quote = TextFormField(
      controller: quoteController,
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Quote',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          children: <Widget>[
            base,
            SizedBox(
              height: 5,
            ),
            quote,
            Padding(
              padding: EdgeInsets.all(8),
              child: FlatButton.icon(
                icon: Icon(Icons.sync),
                onPressed: update,
                label: Text("Get trades"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
