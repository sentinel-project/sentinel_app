import 'package:flutter/material.dart';
import 'package:sembast/sembast.dart';
import 'package:sentinel/models/balances/balances_model.dart';
import 'package:sentinel/models/user/user_model.dart';
import 'package:sentinel/models/authentication/auth_model.dart';
import 'package:sentinel/services/balances_service.dart';
import 'package:sentinel/services/price_service.dart';
import 'package:sentinel/services/quote_service.dart';
import 'package:sentinel/services/user_service.dart';
import 'package:sentinel/widgets/onboarding/onboarding_page.dart';
import 'package:sentinel/widgets/stats/balance_page.dart';
import 'package:sentinel/widgets/stats/evolution_page.dart';
import 'package:sentinel/widgets/user/trades/trades_page.dart';

import 'balances/balances_page.dart';

class UserPage extends StatefulWidget {
  final Auth token;
  final bool darkmode;
  final Function cb;
  final Database db;

  UserPage({Key key, this.darkmode, this.cb, this.db, this.token})
      : super(key: key);
  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  int page = 0;
  User user;
  String errmsg;
  Balances bals;
  bool darkmode;
  bool isupdating = false;
  Brightness ness;

  void init() async {
    try {
      User _user;
      bool iscached =
          await UserService(db: widget.db, token: widget.token).islocalcache();
      if (!iscached) {
        _user = await UserService(token: widget.token).getUser();
        User.save(widget.db, user);
      } else {
        _user = await User.load(widget.db);
      }
      user = _user;
    } catch (e) {
      if (e == Exception(NoBalException)) {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (ctx) => OnboardingPage(
                      token: widget.token,
                      user: user,
                      cb: widget.cb,
                    )));
      } else {
        print(e);
        errmsg = e.toString();
      }
    }
    setState(() {});
  }

  @override
  initState() {
    super.initState();
    darkmode = widget.darkmode;
    ness = darkmode ? Brightness.dark : Brightness.light;
    init();
  }

  Widget _title() => Row(
        children: <Widget>[
          SizedBox(
            width: 10,
          ),
          Text(
            "Sentinel ",
            textScaleFactor: 0.8,
          ),
          Expanded(
            child: Container(),
          ),
          Text(
            "💡",
            textScaleFactor: 0.8,
          ),
          Switch(
            onChanged: (val) {
              darkmode = val;
              ness = darkmode ? Brightness.dark : Brightness.light;
              widget.cb(ness);
              setState(() {});
            },
            value: darkmode,
          ),
          Text(
            "🌙",
            textScaleFactor: 0.8,
          ),
        ],
      );
  List<Widget> _pages() => [
        BalancesPage(
          key: widget.key,
          token: widget.token,
          darkmode: darkmode,
          cb: widget.cb,
          db: widget.db,
        ),
        EvoStatsPage(
          key: widget.key,
          token: widget.token,
          darkmode: darkmode,
          cb: widget.cb,
          db: widget.db,
        ),
        TradesPage(
          key: widget.key,
          token: widget.token,
          darkmode: darkmode,
          cb: widget.cb,
        ),
        BalancesStatsPage(
          key: widget.key,
          token: widget.token,
          darkmode: darkmode,
          cb: widget.cb,
          db: widget.db,
        ),
      ];
  Future<bool> updatePriceAndQuotes() async {
    isupdating = true;
    setState(() {});
    var resP = await PriceService(token: widget.token).updatePrices();
    print(resP);
    var resQ = await QuoteService(token: widget.token).updateQuotes();
    print(resQ);
    isupdating = false;
    setState(() {});
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                child: Center(
                  child: user != null && user.name != null
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("[" + user.name + "]"),
                            Text(user.email),
                            SizedBox(
                              height: 5,
                            ),
                            Text("PORTFOLIO")
                          ],
                        )
                      : Text("PORTFOLIO"),
                ),
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
              ),
              ListTile(
                title: Text('Repartition'),
                onTap: () {
                  // print(this._pages().length);
                  this.page = 3;
                  setState(() {});
                },
              ),
              ListTile(
                title: Text('Item 2'),
                onTap: () {
                  print("coucou");
                },
              ),
            ],
          ),
        ),
        floatingActionButton: page == 0
            ? FloatingActionButton(
                mini: true,
                child: isupdating
                    ? CircularProgressIndicator(
                        strokeWidth: 2,
                      )
                    : Icon(Icons.sync),
                onPressed: () => updatePriceAndQuotes(),
              )
            : null,
        bottomNavigationBar: page <= 2
            ? BottomNavigationBar(
                onTap: (i) => setState(() {
                  print(i);
                  page = i;
                }),
                currentIndex: page,
                items: <BottomNavigationBarItem>[
                  BottomNavigationBarItem(
                      // TODO: fix the white bg on white icons when lightltheme on
                      // backgroundColor: darkmode ? Colors.black : Colors.cyan,
                      title: Text("Assets"),
                      icon: Icon(Icons.account_balance)),
                  BottomNavigationBarItem(
                      title: Text("Estat"), icon: Icon(Icons.change_history)),
                  BottomNavigationBarItem(
                      title: Text("Trades"),
                      icon: Icon(Icons.lightbulb_outline)),
                ],
              )
            : null,
        appBar: AppBar(
          title: _title(),
        ),
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              errmsg == null
                  ? _pages()[page]
                  : Center(
                      child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(" ⚠ " + " : [" + errmsg + "]"),
                    ))
            ],
          ),
        ));
  }
}
