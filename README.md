# Sentinel - Android/Ios client

A beautiful mobile application to setup and monitor Sentinel 

[![Generic badge](https://img.shields.io/badge/stable-v1.1.0-green.svg)](https://shields.io/) [![Generic badge](https://img.shields.io/badge/<BUILD>-<OK>-<COLOR>.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/<TESTS>-<OK>-<COLOR>.svg)](https://shields.io/)

This Android and Ios Flutter app is still 
under heavy reflexion and development, she is trying to stick to the [golangapi](../main.go) features but it might not be allways on the same state. In fact these two programms are develloped conjointly, the advances in one push the ideation and implementation on the other and `vice-versa`.

## 👁  WYSiWYG :

### Login

| LightTheme | DarkTheme |
| -- | -- |
|![LoginWhite](https://imgur.com/acrtsWt.jpg)|![LoginBlack](https://imgur.com/vLRxFRF.jpg)

### Register

| LightTheme | DarkTheme |
| -- | -- |
|![RegisterWhite](https://imgur.com/d8CEtSv.jpg)|![RegisterBlack](https://imgur.com/OTDe5uZ.jpg)|

### OnBoarding

| LightTheme | DarkTheme |
| -- | -- |
|![OnboardingWhite](https://imgur.com/g3VxU9o.jpg)|![OnboardingBlack](https://imgur.com/arj9ibs.jpg) |

### AssetList
| LightTheme | DarkTheme |
| -- | -- |
|![AssetListWhite](https://imgur.com/ZxW3JY9.jpg) |![AssetListBlack](https://imgur.com/vYA0lnP.jpg)|

### AssetDetails
| LightTheme | DarkTheme |
| -- | -- |
| ![AssetDetailWhite](https://imgur.com/B1W4zMX.jpg) |![AssetDetailBlack](https://imgur.com/32zmfNe.jpg)


# Developement 
This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
